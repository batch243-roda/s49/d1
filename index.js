const divPostEntries = document.querySelector("#div-post-entries");
const addPostForm = document.querySelector("#form-add-post");
const title = document.querySelector("#txt-title");
const body = document.querySelector("#txt-body");

const editPostForm = document.querySelector("#form-edit-post");
const editTitle = document.querySelector("#txt-edit-title");
const editBody = document.querySelector("#txt-edit-body");
const editId = document.querySelector("#txt-edit-id");
const updateBtn = document.querySelector("#btn-update");

const fetchPost = async () => {
  const posts = await fetch("https://jsonplaceholder.typicode.com/posts");
  const data = await posts.json();

  showPosts(data);
};

const showPosts = (posts) => {
  let postEntries = "";

  posts.forEach((post) => {
    postEntries += `
      <div id = "post-${post.id}">
        <h3 id = "post-title-${post.id}">${post.title}</h3>
        <p id = "post-body-${post.id}">${post.body}</p>
        <button onclick="editPost('${post.id}')">Edit</button>
        <button onclick="deletePost('${post.id}')">Delete</button>
      </div>
    `;
  });
  divPostEntries.innerHTML = postEntries;
};

// Add post
addPostForm.addEventListener("submit", async (e) => {
  e.preventDefault();

  if (!title.value || !body.value)
    return console.log("Please input Title or body");
  const posts = await fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      title: title.value,
      body: body.value,
      userId: 1,
    }),
  });

  const data = await posts.json();

  if (!data) return alert("Error");

  alert("Added successfully");
  console.log(data);
  title.value = "";
  body.value = "";
});

// Edit post
const editPost = (id) => {
  let postTitle = document.querySelector(`#post-title-${id}`).innerHTML;
  let postBody = document.querySelector(`#post-body-${id}`).innerHTML;

  editTitle.value = postTitle;
  editBody.value = postBody;
  editId.value = id;
  updateBtn.disabled = false;
};

editPostForm.addEventListener("submit", async (e) => {
  e.preventDefault();

  const posts = await fetch(
    `https://jsonplaceholder.typicode.com/posts/${editId.value}`,
    {
      method: "PATCH",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        title: editTitle.value,
        body: editBody.value,
      }),
    }
  );

  const data = await posts.json();

  if (!data) return alert("Error");

  alert("Added successfully");
  console.log(data);
  editTitle.value = "";
  editBody.value = "";
  updateBtn.disabled = true;
});

// Delete post
const deletePost = async (id) => {
  confirm("Are you sure you want to delete?");
  const posts = await fetch(
    `https://jsonplaceholder.typicode.com/posts/${id}`,
    {
      method: "DELETE",
    }
  );
  const data = await posts.json();

  if (!data) return alert("Error");

  alert("Deleted successfully");
  document.querySelector(`#post-${id}`).remove();
};

fetchPost();
